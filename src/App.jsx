import './App.css';
import Bottom from './components/Bottom/Bottom';
import Hero from './components/Hero/Hero';
import { Outlet } from 'react-router-dom';


function App() {
  return (
    <>
      <Hero />
      <Bottom />
    </>
  );
}

export default App;
