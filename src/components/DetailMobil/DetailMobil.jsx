import BottomDetail from "../BottomDetail/BottomDetail";
import Top from "../Top/Top";

const DetailMobil = () => {
  return (
    <>
        <Top />
        <BottomDetail />
    </>
  )
}

export default DetailMobil;