import React from "react";
import style from '../CariMobil/CariMobil.module.css';
import { useNavigate } from "react-router-dom";
import { dataCarAction } from "../../redux/actions/dataCarAction";
import { useDispatch } from "react-redux";

const CariMobil = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    dispatch(dataCarAction());

    return (
        <>
            <div className={style.cariMobilContainer}>
                <div className={style.cariMobil}>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Tipe Driver</p>
                        </div>
                        <div>
                            <select name="tipeDriver" id="tipeDriver" placeholder="Pilih Tipe Driver">
                                <option value="sopir">Dengan Sopir</option>
                                <option value="tanpa-sopir">Tanpa Sopir (Lepas Kunci)</option>
                            </select>
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Tanggal</p>
                        </div>
                        <div className={style.date}>
                            <input id="date" type="date"/>
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Waktu Jemput/Ambil</p>
                        </div>
                        <div className={style.date}>
                            <input type="time" />
                        </div>
                    </div>
                    <div className={style.poinCariMobil}>
                        <div>
                            <p>Jumlah Penumpang (optional)</p>
                        </div>
                        <div className={style.penumpang}>
                            <input placeholder="Jumlah Penumpang" type="number" />
                        </div>
                    </div>
                    <div>
                        <button 
                            className={style.buttonCariMobil}
                            onClick={() => {navigate('/search-result')}}>
                            Cari Mobil
                        </button>
                    </div>
                </div>
            </div>
        </>
    )
}

export default CariMobil;